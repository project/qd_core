<?php
/**
 * @file
 * Code for the QD Core Distance logic.
 */


/**
 * Gets the last imported distance record.
 *
 * @return Object
 *   The last recorded distance record (timestamp, source_id only)
 */
function qd_core_distance_get_latest_entry($uid, $service_id) {
  $query = 'SELECT id, timestamp, source_id from {qd_core_distance} ';
  $query .= 'WHERE uid=:uid AND service_id=:service_id ';
  $query .= 'ORDER BY timestamp DESC LIMIT 1';

  $params = array(
    ':uid' => $uid,
    ':service_id' => $service_id,
  );

  $result = db_query($query, $params);

  return $result->fetchObject();
}


function qd_core_distance_create($uid, $timestamp, $utc_offset, $source_id, $distance, $service_id) {
  $entity = entity_create('qd_core_distance', array('type' => 'qd_core_distance'));
  $entity->uid = $uid;
  $entity->timestamp = $timestamp;
  $entity->utc_offset = $utc_offset;
  $entity->source_id = $source_id;
  $entity->distance = $distance;
  $entity->service_id = $service_id;

  return $entity;
}



function qd_core_distance_update($uuid, $uid, $timestamp, $utc_offset, $source_id, $distance, $service_id) {
  // TODO:
}


function qd_core_distance_delete($uuid) {
  // TODO:
}


/**
 * Implements hook_views_api().
 */
function qd_core_distance_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'qd_core_distance') . '/includes/views',
    'template path' => drupal_get_path('module', 'qd_core_distance') . '/themes',
  );
}


/**
 * Implements hook_entity_info().
 */
function qd_core_distance_entity_info() {
  $info = array(
    'qd_core_distance' => array(
      'label' => t('QD Core Distance'),
      'entity class' => 'QDCoreDistance',
      'controller class' => 'QDCoreDistanceController',
      'base table' => 'qd_core_distance',
      'uri callback' => 'qd_core_distance_uri',
      'fieldable' => TRUE,
      'entity keys' => array(
        'id' => 'id',
        'uuid' => 'uuid',
      ),
      'static cache' => TRUE,
      'views controller class' => 'EntityDefaultViewsController',
      'uuid' => TRUE,
    ),
  );

  return $info;
}


/**
 * Implements hook_entity_property_info_alter().
 */
function qd_core_distance_entity_property_info_alter(&$info) {
  $properties = &$info['qd_core_distance']['properties'];

  $properties['id'] = array(
    'label' => 'QD Distance summary ID',
    'description' => t('The unique ID for the qd_core_distance.'),
    'type' => 'integer',
    'schema field' => 'id',
  );
  $properties['uuid'] = array(
    'label' => 'QD Activity UUID',
    'description' => t('UUID'),
    'type' => 'integer',
    'schema field' => 'id',
  );
  $properties['timestamp'] = array(
    'label' => 'Timestamp',
    'description' => t('The timestamp of when the measurement was taken.'),
    'type' => 'date',
    'schema field' => 'timestamp',
  );
  $properties['utc_offset'] = array(
    'label' => 'UTC Offset',
    'description' => t('The timestamp offset from UTC.'),
    'type' => 'integer',
    'schema field' => 'utc_offset',
  );
  $properties['uid'] = array(
    'label' => 'User ID',
    'description' => t('The user ID this value belongs to.'),
    'type' => 'user',
    'schema field' => 'uid',
  );
  $properties['service_id'] = array(
    'label' => 'Service ID',
    'description' => t('The tid of the service (taxonomy) this value came from.'),
    'type' => 'integer',
    'schema field' => 'service_id',
  );
  $properties['source_id'] = array(
    'label' => 'Source ID',
    'description' => t('The id from the service that identifies this record on that source service.'),
    'type' => 'text',
    'schema field' => 'source_id',
  );
  $properties['distance'] = array(
    'label' => 'Distance',
    'description' => t('The calculated distance traveled this day (km).'),
    'type' => 'float',
    'schema field' => 'distance',
  );
  return $properties;
}
