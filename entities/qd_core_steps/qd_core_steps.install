<?php
/**
 * @file
 * Code for the QD Core Steps installation.
 */


/**
 * Implements hook_enable().
 */
function qd_core_steps_enable() {
  uuid_sync_all();
}


/**
 * Implements hook_schema().
 */
function qd_core_steps_schema() {
  $schema['qd_core_steps'] = array(
    'description' => 'The table for quantified data steps count.',
    'fields' => array(
      'id' => array(
        'description' => 'The steps id.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'uuid' => uuid_schema_field_definition(),
      'timestamp' => array(
        'description' => 'The timestamp of when the measurement was taken.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'utc_offset' => array(
        'description' => 'The timestamp offset from UTC.',
        'type' => 'int',
        'unsigned' => FALSE,
        'not null' => TRUE,
      ),
      'uid' => array(
        'description' => 'The user ID this value belongs to.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'service_id' => array(
        'description' => 'The tid of the service (taxonomy) this value came from.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'source_id' => array(
        'description' => 'The ID from the service that identifies this record on that source service.',
        'type' => 'varchar',
        'length' => 256,
        'not null' => FALSE,
      ),
      'total_steps' => array(
        'description' => 'The total steps counted.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('id'),
  );

  return $schema;
}
